package com.XtraordinairDEV.batteryhomebutton;

import android.widget.ImageView;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by Steph on 6/26/2016.
 */
public class XposedDUHomeButtonMod implements IXposedHookLoadPackage {
    private final String SYSTEM_UI = "com.android.systemui";
    private final String SMARTBAR_PACKAGE = SYSTEM_UI + ".navigation.smartbar";
    private BatteryDrawable mBatteryDrawable;

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        if (!lpparam.packageName.equals(SYSTEM_UI))
            return;

        Class<?> SmartBarView;
        try {
            SmartBarView =
                    XposedHelpers.findClass(SMARTBAR_PACKAGE + ".SmartBarView",
                            lpparam.classLoader);
        }catch (Exception e){
            XposedBridge.log("BHI - Failed to find SmartBarView");
            return;
        }

        XposedBridge.log("BHI - Point of no return");
        XC_MethodHook hook = new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                ImageView imageView = (ImageView) XposedHelpers.callMethod(param.thisObject, "getHomeButton");
                replaceHomeIcon(imageView);
            }
        };

        XposedHelpers.findAndHookMethod(SmartBarView, "onInflateFromUser", hook);
        XposedHelpers.findAndHookMethod(SmartBarView, "onRecreateStatusbar", hook);
        XposedHelpers.findAndHookMethod(SmartBarView, "reorient", hook);
        XposedHelpers.findAndHookMethod(SmartBarView, "recreateLayouts", hook);
        XposedHelpers.findAndHookMethod(SmartBarView, "updateCurrentIcons", hook);

        XposedBridge.hookAllConstructors(SmartBarView, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                ImageView imageView = (ImageView) XposedHelpers.callMethod(param.thisObject, "getHomeButton");
                replaceHomeIcon(imageView);
            }
        });
    }

    private void replaceHomeIcon(ImageView imageView){
        if(mBatteryDrawable == null)
            mBatteryDrawable = BatteryDrawable.newInstance(imageView, BatteryDrawable.SHOW_PERCENTAGE);
        imageView.setImageDrawable(mBatteryDrawable);
        mBatteryDrawable.setView(imageView);
        ImageViewHelper imageViewHelper = new ImageViewHelper(imageView, mBatteryDrawable);
        imageViewHelper.setReceiver();
        imageViewHelper.onWindowEvent();
    }
}