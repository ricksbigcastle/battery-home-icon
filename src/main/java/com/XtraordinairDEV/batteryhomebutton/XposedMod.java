package com.XtraordinairDEV.batteryhomebutton;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import java.util.Locale;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.XposedHelpers.ClassNotFoundError;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class XposedMod implements IXposedHookLoadPackage {
	private BatteryDrawable mBatteryDrawable;
	private final String SYSTEMUI = "com.android.systemui";
	private final String SYSTEMUI_STATUSBAR_PHONE = SYSTEMUI + ".statusbar.phone";

	@Override
	public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {
		if (!lpparam.packageName.equals(SYSTEMUI))
			return;

		boolean isLGDevice = Build.MANUFACTURER.toLowerCase(Locale.getDefault()).equals("lge");
		Class<?> NavigationBarView;

		if (!isLGDevice) {
			NavigationBarView = 
					XposedHelpers.findClass(SYSTEMUI_STATUSBAR_PHONE + ".NavigationBarView",
							lpparam.classLoader);
		}else {
			// User might be using CM or something on an LG manufactured device
			try {
				NavigationBarView =
						XposedHelpers.findClass(SYSTEMUI_STATUSBAR_PHONE + ".LGNavigationBarView",
								lpparam.classLoader);
			} catch (ClassNotFoundError e) {
				NavigationBarView =
						XposedHelpers.findClass(SYSTEMUI_STATUSBAR_PHONE + ".NavigationBarView",
								lpparam.classLoader);
				//Assuming device is LG device, but has flashed ROM or something
				isLGDevice = false;
			}
		}

		XC_MethodHook hook = new XC_MethodHook() {
			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable {
				try {
					ImageView imageView = (ImageView) XposedHelpers.callMethod(param.thisObject, "getHomeButton");
					replaceHomeIcon(imageView);
				} catch (NoSuchMethodError e) {
					// ROMs with editable nav bar
					try {
						Class<?> NavBarEditor = XposedHelpers.findClass(SYSTEMUI_STATUSBAR_PHONE + ".NavbarEditor",
								param.thisObject.getClass().getClassLoader());
						ImageView imageView = null;
						XposedBridge.log("BHI - NavBar Checkpoint 2");

						// CyanogenMod
						try {
							imageView = (ImageView) XposedHelpers.callMethod(param.thisObject, "findButton",
									XposedHelpers.getStaticObjectField(NavBarEditor, "NAVBAR_HOME"));
							XposedBridge.log("BHI - NavBar Checkpoint 3");
						} catch (NoSuchMethodError e2) {
							// OmniROM
							try {
								View mCurrentView = (View) XposedHelpers.getObjectField(param.thisObject, "mCurrentView");
								imageView = (ImageView) mCurrentView.findViewWithTag(
										XposedHelpers.getStaticObjectField(NavBarEditor, "NAVBAR_HOME"));
								XposedBridge.log("BHI - NavBar Checkpoint 4");
							} catch (Exception e3) {

							}
						}

						if (imageView != null) {
							replaceHomeIcon(imageView);
							XposedBridge.log("BHI - NavBar Checkpoint 7");
						}else{
							XposedBridge.log("BHI - NavBar Checkpoint 8");
						}
					} catch (Exception e1) {
						XposedBridge.log("BHI - NavBar Checkpoint 9");
					}
				}
			}
		};
		XposedHelpers.findAndHookMethod(NavigationBarView, "onFinishInflate", hook);
		XposedHelpers.findAndHookMethod(NavigationBarView, "reorient", hook);

		XposedBridge.hookAllConstructors(NavigationBarView, new XC_MethodHook() {
			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable {
				ImageView imageView = (ImageView) XposedHelpers.callMethod(param.thisObject, "getHomeButton");
				replaceHomeIcon(imageView);
			}
		});

		
		if (!isLGDevice) {
			XposedHelpers.findAndHookMethod(NavigationBarView, "getIcons", Resources.class, new XC_MethodHook() {
				@Override
				protected void afterHookedMethod(MethodHookParam param) throws Throwable {
					try {
						XposedHelpers.setObjectField(param.thisObject, "mHomeIcon", mBatteryDrawable);
						XposedHelpers.setObjectField(param.thisObject, "mHomeLandIcon", mBatteryDrawable);
						XposedBridge.log("BHI - NavBar Checkpoint 10");
					} catch (NoSuchFieldError e) {
						XposedBridge.log("BHI - getIcons failed.");
					}
				}
			});
		}else{
			stockLGReplace(lpparam);
		}
	}

	private void replaceHomeIcon(ImageView imageView){
		if(mBatteryDrawable == null)
			mBatteryDrawable = BatteryDrawable.newInstance(imageView, BatteryDrawable.SHOW_PERCENTAGE);
		imageView.setImageDrawable(mBatteryDrawable);
		mBatteryDrawable.setView(imageView);
		ImageViewHelper imageViewHelper = new ImageViewHelper(imageView, mBatteryDrawable);
		imageViewHelper.setReceiver();
		imageViewHelper.onWindowEvent();
	}

	/***********************************************************************************************
	 * STOCK LG START
     **********************************************************************************************/

	private void stockLGReplace(LoadPackageParam lpparam){
		String navBarLG = "com.lge.navigationbar";

		try {
			Class<?> LGHomeButton = XposedHelpers.findClass(navBarLG + ".HomeButton", lpparam.classLoader);
			XposedHelpers.findAndHookConstructor(LGHomeButton, Context.class, AttributeSet.class, int.class, boolean.class, new XC_MethodHook() {
				@Override
				protected void afterHookedMethod(MethodHookParam param) throws Throwable {
					ImageView imageView = (ImageView) param.thisObject;
					if(mBatteryDrawable == null)
						mBatteryDrawable = BatteryDrawable.newInstance(imageView,BatteryDrawable.SHOW_PERCENTAGE);

					imageView.setImageDrawable(mBatteryDrawable);
				}
			});
		} catch (Throwable t) {
			XposedBridge.log("BatteryHomeIcon: Failed to apply LG hook: " + t.getMessage());
			t.printStackTrace();
		}

		try {
			Class<?> NavigationThemeResource = XposedHelpers.findClass(navBarLG + ".NavigationThemeResource", lpparam.classLoader);
			XposedHelpers.findAndHookMethod(NavigationThemeResource, "getThemeResource", View.class, new XC_MethodHook() {
				@Override
				protected void afterHookedMethod(MethodHookParam param) throws Throwable {
					try {
						ImageView imageView = (ImageView)param.args[0];
						if (imageView.getClass().getSimpleName().equalsIgnoreCase("HomeButton")) {
							if(mBatteryDrawable == null)
								mBatteryDrawable = BatteryDrawable.newInstance(imageView, BatteryDrawable.SHOW_PERCENTAGE);
							param.setResult(mBatteryDrawable);
						}
					} catch (NoSuchFieldError e) {
					}
				}
			});
		} catch (Throwable t) {
			XposedBridge.log("BatteryHomeIcon: Failed to apply LG hook: " + t.getMessage());
			t.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * STOCK LG END
	 **********************************************************************************************/
}
