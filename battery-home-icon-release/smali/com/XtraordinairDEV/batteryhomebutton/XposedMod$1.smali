.class Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XposedMod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;


# direct methods
.method constructor <init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V
    .locals 0
    .param p1, "this$0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected afterHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 12
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 71
    :try_start_0
    iget-object v7, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v8, "getHomeButton"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 72
    .local v5, "imageView":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # invokes: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->createBatteryIfNeeded(Landroid/widget/ImageView;)V
    invoke-static {v7, v5}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$000(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;Landroid/widget/ImageView;)V

    .line 73
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v7}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 74
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v7}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setView(Landroid/widget/ImageView;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v5    # "imageView":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v2

    .line 78
    .local v2, "e":Ljava/lang/NoSuchMethodError;
    :try_start_1
    const-string v7, "com.android.systemui.statusbar.phone.NavbarEditor"

    iget-object v8, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    .line 79
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    .line 78
    invoke-static {v7, v8}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 80
    .local v1, "NavBarEditor":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    .line 83
    .restart local v5    # "imageView":Landroid/widget/ImageView;
    :try_start_2
    iget-object v7, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v8, "findButton"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "NAVBAR_HOME"

    .line 84
    invoke-static {v1, v11}, Lde/robv/android/xposed/XposedHelpers;->getStaticObjectField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    .line 83
    invoke-static {v7, v8, v9}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Landroid/widget/ImageView;

    move-object v5, v0
    :try_end_2
    .catch Ljava/lang/NoSuchMethodError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 96
    :goto_1
    if-eqz v5, :cond_0

    .line 97
    :try_start_3
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # invokes: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->createBatteryIfNeeded(Landroid/widget/ImageView;)V
    invoke-static {v7, v5}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$000(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;Landroid/widget/ImageView;)V

    .line 98
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v7}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    iget-object v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$1;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v7}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setView(Landroid/widget/ImageView;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 101
    .end local v1    # "NavBarEditor":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "imageView":Landroid/widget/ImageView;
    :catch_1
    move-exception v7

    goto :goto_0

    .line 85
    .restart local v1    # "NavBarEditor":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v5    # "imageView":Landroid/widget/ImageView;
    :catch_2
    move-exception v3

    .line 88
    .local v3, "e2":Ljava/lang/NoSuchMethodError;
    :try_start_4
    iget-object v7, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v8, "mCurrentView"

    invoke-static {v7, v8}, Lde/robv/android/xposed/XposedHelpers;->getObjectField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 89
    .local v6, "mCurrentView":Landroid/view/View;
    const-string v7, "NAVBAR_HOME"

    .line 90
    invoke-static {v1, v7}, Lde/robv/android/xposed/XposedHelpers;->getStaticObjectField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 89
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Landroid/widget/ImageView;

    move-object v5, v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    .line 91
    .end local v6    # "mCurrentView":Landroid/view/View;
    :catch_3
    move-exception v4

    .line 92
    .local v4, "e3":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1
.end method
